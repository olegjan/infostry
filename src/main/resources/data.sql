-- USER --
INSERT ignore INTO user SET
first_name='test', last_name='test', email='test@mail.com', phone_number='0991234567', password='$2a$10$9LzxXAfO5RZMpmQR05TuvO0HXM2uBOcONoigTiiAAQvj.dZlZin1y';

-- ROLE --
INSERT ignore INTO role SET name='USER';

-- USER ROLE --
INSERT ignore INTO user_role SET
user_id=(SELECT id FROM user WHERE email='test@mail.com'),
role_id=(SELECT id FROM role WHERE name='USER')