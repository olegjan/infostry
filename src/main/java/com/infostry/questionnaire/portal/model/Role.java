package com.infostry.questionnaire.portal.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    private String name;

    /**
     * Association to the User entity: these users have the role (this)
     */
    @ManyToMany(mappedBy = "roles")
    private Set<User> users = new HashSet<>();

    /**
     * Noargs constructor
     */
    public Role() {
        super();
    }

    /**
     * Constructor for creation of a role with a specific name
     * @param roleName role name
     */
    public Role(String roleName) {
        super();
        this.setName(roleName);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter for user list
     * @return user list
     */
    public Set<User> getUsers() {
        return users;
    }

    /**
     * setter for user list
     * @param users user list
     */
    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
