package com.infostry.questionnaire.portal.service.impl;

import com.infostry.questionnaire.portal.model.Role;
import com.infostry.questionnaire.portal.model.User;
import com.infostry.questionnaire.portal.repository.RoleRepository;
import com.infostry.questionnaire.portal.repository.UserRepository;
import com.infostry.questionnaire.portal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        HashSet<Role> roles = new HashSet<>(roleRepository.findAll());
        user.setRoles(new HashSet<>(roles));
        userRepository.save(user);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}
