package com.infostry.questionnaire.portal.service;

import com.infostry.questionnaire.portal.model.User;

public interface UserService {
    void save(User user);
    User findByEmail(String email);
}
