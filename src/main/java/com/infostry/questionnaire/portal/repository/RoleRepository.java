package com.infostry.questionnaire.portal.repository;

import com.infostry.questionnaire.portal.model.Role;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    /**
     * Get role by id
     * @param aLong id
     * @return role object
     */
    @Override
    Role getOne(Long aLong);

    /**
     * Find role by id
     * @param aLong id
     * @return role object
     */
    @Override
    Optional<Role> findById(Long aLong);

    /**
     * Find role by name
     *
     * @param rolename name of role
     * @return role based on roleName
     */
    Role findByName(String rolename);

    /**
     * Find all
     * @return list of role objects
     */
    @Override
    List<Role> findAll();

    /**
     * Find all with sort
     * @param sort sort object
     * @return list of role objects
     */
    @Override
    List<Role> findAll(Sort sort);

    /**
     * Find all by exmaple
     * @param example query object
     * @param <S> role / derived class
     * @return list of role objects
     */
    @Override
    <S extends Role> List<S> findAll(Example<S> example);

    /**
     * Delete by id
     * @param aLong id
     */
    @Override
    void deleteById(Long aLong);

    /**
     * Delete role object
     * @param role role object
     */
    @Override
    void delete(Role role);

    /**
     * save role
     * @param s role / derived object
     * @param <S> role / derived class
     * @return saved role / derived object
     */
    @Override
    <S extends Role> S save(S s);

    /**
     * save role and flush
     * @param s role / derived object
     * @param <S> role / derived class
     * @return saved role / derived object
     */
    @Override
    <S extends Role> S saveAndFlush(S s);

    /**
     * get number of role objects
     * @return number of role objects
     */
    @Override
    long count();
}
