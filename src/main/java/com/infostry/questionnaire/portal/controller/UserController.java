package com.infostry.questionnaire.portal.controller;

import com.infostry.questionnaire.portal.model.User;
import com.infostry.questionnaire.portal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        return "login";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model, String error, String logout) {
        model.addAttribute("userForm", new User());
        return "registration";
    }

    @RequestMapping(value="/registration", method = RequestMethod.POST)
    public String createUser(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        System.out.println(userForm);
        userService.save(userForm);
        return "redirect:/login";
    }
}
