package com.infostry.questionnaire.portal;


import com.infostry.questionnaire.portal.security.WebSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * The Application starter using spring boot engine
 */
@SpringBootApplication
@Import(WebSecurityConfig.class)
public class Application {
    /**
     * Entry point for running application
     *
     * @param args additional running params
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
